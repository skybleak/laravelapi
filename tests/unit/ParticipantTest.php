<?php

namespace Tests\Unit;

use App\Events;
use App\Participant;
use Faker\Factory;
use GuzzleHttp\Client;
use TestCase;

class ParticipantTest extends TestCase
{
    public function test_can_create_post()
    {
        $http = new Client();
        $response = $http->request('POST', 'http://localhost:8080/api/participant', [
            'form_params' => [
                'name' => 'sdasdsad',
                'surname' => 'sadsa',
                'email' => 'asdsad@sdfsdf.ru',
                'id_event' => 1,
                'key' => config('constants.api_key')['participant']
            ]
        ]);

        $response_array = json_decode((string)$response->getBody(), true);
        dd($response_array);
    }

    public function test_can_update_post()
    {
        $http = new Client();
        $response = $http->request('PUT', 'http://localhost:8080/api/participant/1', [
            'form_params' => [
                'name' => 'test',
                'surname' => 'test',
                'email' => 'asdsad@sdfsdf.ru',
                'id_event' => 1,
                'key' => config('constants.api_key')['participant']
            ]
        ]);

        $response_array = json_decode((string)$response->getBody(), true);
        dd($response_array);
    }
}