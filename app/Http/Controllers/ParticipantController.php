<?php

namespace App\Http\Controllers;

use App\Events;
use App\Helpers\Contracts\ParticipantInterface;
use App\Http\Requests\StoreParticipantPost;
use App\Http\Requests\UpdateParticipantPut;
use App\Jobs\SendReminderEmail;
use App\Notification;
use App\Participant;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Http\Request;

class ParticipantController extends Controller implements ParticipantInterface
{
    public function show(Events $events)
    {
        $participants = Participant::query()
            ->where('id_event', $events->id)
            ->get();

        return response()->json($participants, 200);
    }

    public function store(StoreParticipantPost $request)
    {

        try {
            $participant = Participant::create($request->all());
            dispatch(new SendReminderEmail($participant));

            return response()->json(['success' => true, 'msg' => 'Success to create'], 201);
        } catch (QueryException $queryException) {
            throw new HttpResponseException(response()->json(['success' => false, 'msg' => 'Failed to create'], 422));
        }
    }

    public function update(UpdateParticipantPut $request, Participant $participant)
    {
        try {
            $participant->update($request->all());
            return response()->json($participant);
        } catch (QueryException $queryException) {
            throw new HttpResponseException(response()->json(['success' => false, 'msg' => 'Failed to update'], 422));
        }
    }

    public function delete(int $id)
    {
        $participant = Participant::find($id);

        if (empty($participant)){
            throw new HttpResponseException(response()->json(['success' => false, 'msg' => 'participant not found']));
        }

        $participant->delete();

        return response()->json(['success' => true, 'msg' => 'Success to delete'], 204);
    }
}
