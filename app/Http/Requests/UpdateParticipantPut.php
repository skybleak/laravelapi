<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exception\HttpResponseException;

class UpdateParticipantPut extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|max:45',
            'surname' => 'string|max:45',
            'email' => 'email|unique:participants|max:255',
            'id_event' => 'int',
        ];
    }

    protected function failedValidation($validator)
    {
        throw new HttpResponseException(response()->json(['success' => false, 'error' => $validator->errors()->first()], 422));
    }
}
