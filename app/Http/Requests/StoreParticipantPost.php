<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exception\HttpResponseException;

class StoreParticipantPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:45',
            'surname' => 'required|string|max:45',
            'email' => 'required|email|unique:participants|max:255',
            'id_event' => 'required|int',
        ];
    }

    protected function failedValidation($validator)
    {
        throw new HttpResponseException(response()->json(['success' => false, 'error' => $validator->errors()->first()], 422));
    }

    public function messages()
    {
        return [
            'name.required' => 'A name is required',
            'surname.required' => 'A surname is required',
            'city.required' => 'A city is required',
            'id_event.required' => 'A id_event is required',
        ];
    }
}
