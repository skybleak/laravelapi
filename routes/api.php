<?php

use Illuminate\Http\Request;
use App\Http\Controllers\ParticipantController;

Route::group(['middleware' => 'api.participant'], function() {
    Route::get('/participant/{events}', 'ParticipantController@show')->name('participant');
    Route::post('/participant', 'ParticipantController@store')->name('participant.store');
    Route::put('/participant/{participant}', 'ParticipantController@update')->name('participant.update');
    Route::delete('/participant/{id}', 'ParticipantController@delete')->name('participant.delete');
});
